defmodule PatronatoWeb.Resolvers.People do
  alias Patronato.Peoples

  def get(%{id: people_id}, _context), do: Peoples.Get.call(people_id)
  def create(%{input: params}, _context), do: Peoples.Create.call(params)
end
