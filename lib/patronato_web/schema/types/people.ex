defmodule PatronatoWeb.Schema.Types.People do
  use Absinthe.Schema.Notation

  object :people do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :cpf, non_null(:string)
  end

  input_object :create_people_input do
    field :name, non_null(:string)
    field :cpf, non_null(:string)
  end
end
