defmodule PatronatoWeb.Schema.Types.Root do
  use Absinthe.Schema.Notation

  alias PatronatoWeb.Resolvers.People, as: PeopleResolver

  import_types PatronatoWeb.Schema.Types.People

  object :root_query do
    field :get_people, type: :people do
      arg :id, non_null(:id)

      resolve fn param1, param2 -> PeopleResolver.get(param1, param2) end
      # resolve &PeopleResolver.get/2
    end
  end

  object :root_mutation do
    field :create_people, type: :people do
      arg :input, non_null(:create_people_input)

      # resolve fn param1, param2 -> PeopleResolver.create(param1, param2) end
      resolve &PeopleResolver.create/2
    end
  end
end
