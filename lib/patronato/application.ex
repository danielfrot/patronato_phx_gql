defmodule Patronato.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Patronato.Repo,
      # Start the Telemetry supervisor
      PatronatoWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Patronato.PubSub},
      # Start the Endpoint (http/https)
      PatronatoWeb.Endpoint
      # Start a worker by calling: Patronato.Worker.start_link(arg)
      # {Patronato.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Patronato.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    PatronatoWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
