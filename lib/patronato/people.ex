defmodule Patronato.People do
  use Ecto.Schema
  import Ecto.Changeset

  schema "peoples" do
    field :cpf, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(attrs) do
    # people
    %__MODULE__{}
    |> cast(attrs, [:name, :cpf])
    |> validate_required([:name, :cpf])
  end
end
