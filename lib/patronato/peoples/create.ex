defmodule Patronato.Peoples.Create do
  alias Patronato.{People, Repo}


  def call(params) do
    params
    |> People.changeset()
    # |> IO.inspect()
    |> Repo.insert()
  end
end
