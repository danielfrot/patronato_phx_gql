defmodule Patronato.Peoples.Get do
  alias Patronato.{People, Repo}


  def call(id) do
    case Repo.get(People, id) do
      nil -> {:error, "Registro não encontrado"}
      people -> {:ok, people}
    end
  end
end
