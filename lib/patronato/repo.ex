defmodule Patronato.Repo do
  use Ecto.Repo,
    otp_app: :patronato,
    adapter: Ecto.Adapters.Postgres
end
