defmodule Patronato.Repo.Migrations.CreatePeoples do
  use Ecto.Migration

  def change do
    create table(:peoples) do
      add :name, :string
      add :cpf, :string

      timestamps()
    end
  end
end
